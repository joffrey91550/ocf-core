package courseController

import (
	"net/http"

	"soli/formations/src/auth/errors"

	"github.com/gin-gonic/gin"
	"github.com/google/uuid"
)

// Get course from chapter godoc
//
//	@Summary		Récupération d'un cours grâce à un chapitre
//	@Description	Récupération des informations du cours grâce à un chapitre
//	@Tags			courses
//	@Accept			json
//	@Produce		json
//	@Param		 	id	path		int	true	"ID chapter"
//	@Param Authorization header string true "Insert your access token" default(bearer <Add access token here>)
//	@Success		200	{object}	dto.CourseOutput
//
//	@Failure		400	{object}	errors.APIError	"Impossible de parser le json"
//	@Failure		404	{object}	errors.APIError	"Course inexistant - Impossible de le récupérer"
//
//	@Router			/courses/fromChapter/{id} [get]
func (courseController courseController) GetCourseFromChapter(ctx *gin.Context) {

	id, err := uuid.Parse(ctx.Param("id"))

	if err != nil {
		ctx.JSON(http.StatusBadRequest, &errors.APIError{
			ErrorCode:    http.StatusBadRequest,
			ErrorMessage: err.Error(),
		})
		return
	}

	course, courseError := courseController.service.GetCourseFromChapter(id)

	if courseError != nil {
		ctx.JSON(http.StatusNotFound, &errors.APIError{
			ErrorCode:    http.StatusNotAcceptable,
			ErrorMessage: err.Error(),
		})
		return
	}

	ctx.JSON(http.StatusOK, *course)
}
