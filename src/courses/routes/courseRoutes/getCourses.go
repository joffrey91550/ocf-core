package courseController

import (
	"net/http"
	"soli/formations/src/auth/errors"

	"github.com/gin-gonic/gin"
)

// Get all courses godoc
//
//	@Summary		Récupération des cours
//	@Description	Récupération de tous les courses dans la base données
//	@Tags			courses
//	@Accept			json
//	@Produce		json
//
//	@Param Authorization header string true "Insert your access token" default(bearer <Add access token here>)
//
//	@Success		200	{object}	[]dto.CourseOutput
//
//	@Failure		404	{object}	errors.APIError	"Courses inexistants"
//
//	@Router			/courses [get]
func (courseController courseController) GetCourses(ctx *gin.Context) {

	courses, err := courseController.service.GetCourses()

	if err != nil {
		ctx.JSON(http.StatusNotFound, &errors.APIError{
			ErrorCode:    http.StatusNotFound,
			ErrorMessage: "Course not found",
		})
		return
	}

	ctx.JSON(http.StatusOK, courses)
}
