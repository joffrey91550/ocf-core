package courseController

import (
	"fmt"
	"net/http"

	"soli/formations/src/auth/errors"

	"github.com/gin-gonic/gin"
	"github.com/google/uuid"
)

// Get course from section godoc
//
//	@Summary		Récupération d'une section
//	@Description	Récupération des informations d'une section
//	@Tags			courses
//	@Accept			json
//	@Produce		json
//	@Param		 	id	path		int	true	"ID section"
//	@Param Authorization header string true "Insert your access token" default(bearer <Add access token here>)
//	@Success		200	{object}	dto.CourseOutput
//
//	@Failure		400	{object}	errors.APIError	"Impossible de parser le json"
//	@Failure		404	{object}	errors.APIError	"Course inexistant - Impossible de le récupérer"
//
//	@Router			/courses/fromSection/{id} [get]
func (courseController courseController) GetSection(ctx *gin.Context) {
	id, err := uuid.Parse(ctx.Param("id"))

	if err != nil {
		ctx.JSON(http.StatusBadRequest, &errors.APIError{
			ErrorCode:    http.StatusBadRequest,
			ErrorMessage: err.Error(),
		})
		return
	}

	section, err := courseController.service.GetSection(id)

	fmt.Println(section)

	if err != nil {
		ctx.JSON(http.StatusNotFound, &errors.APIError{
			ErrorCode:    http.StatusNotAcceptable,
			ErrorMessage: err.Error(),
		})
		return
	}

	ctx.JSON(http.StatusOK, *section)
}
