package repositories

import (
	"soli/formations/src/courses/dto"
	"soli/formations/src/courses/models"

	authDtos "soli/formations/src/auth/dto"
	authModels "soli/formations/src/auth/models"

	"github.com/google/uuid"
	"gorm.io/gorm"
)

type CourseRepository interface {
	CreateCourse(coursedto dto.CreateCourseInput) (*models.Course, error)
	DeleteCourse(id uuid.UUID) error
	GetSpecificCourseByUser(owner authModels.User, courseName string) (*models.Course, error)
	GetCourse(id uuid.UUID) (*models.Course, error)
	GetCourseFromChapter(id uuid.UUID) (*models.Course, error)
	GetSection(id uuid.UUID) (*models.Section, error)
	GetAllCourses() (*[]models.Course, error)
}

type courseRepository struct {
	db *gorm.DB
}

func NewCourseRepository(db *gorm.DB) CourseRepository {
	repository := &courseRepository{
		db: db,
	}
	return repository
}

func (c courseRepository) CreateCourse(coursedto dto.CreateCourseInput) (*models.Course, error) {

	var user *authModels.User
	errUser := c.db.First(&user, "email=?", coursedto.AuthorEmail)
	if errUser.Error != nil {
		return nil, errUser.Error
	}

	userDto := authDtos.UserModelToUserOutput(*user)

	//ToDo full course with dtoinput to model
	course := models.Course{
		Name:               coursedto.Name,
		Theme:              coursedto.Theme,
		Owner:              userDto,
		OwnerID:            &user.ID,
		Category:           coursedto.Category,
		Version:            coursedto.Version,
		Title:              coursedto.Title,
		Subtitle:           coursedto.Subtitle,
		Header:             coursedto.Header,
		Footer:             coursedto.Footer,
		Logo:               coursedto.Logo,
		Description:        coursedto.Description,
		Format:             models.Format(coursedto.Format),
		CourseID_str:       coursedto.CourseID_str,
		Schedule:           coursedto.Schedule,
		Prelude:            coursedto.Prelude,
		LearningObjectives: coursedto.LearningObjectives,
		Chapters:           coursedto.Chapters,
	}

	result := c.db.Create(&course)
	if result.Error != nil {
		return nil, result.Error
	}
	return &course, nil
}

func (c courseRepository) DeleteCourse(id uuid.UUID) error {
	result := c.db.Delete(&models.Course{ID: id})
	if result.Error != nil {
		return result.Error
	}
	return nil
}

func (c courseRepository) GetSpecificCourseByUser(owner authModels.User, courseName string) (*models.Course, error) {
	var course *models.Course
	err := c.db.Preload("Chapters").Preload("Chapters.Sections").First(&course, "owner_id=? AND name=?", owner.ID.String(), courseName).Error
	if err != nil {
		return nil, err
	}
	return course, nil
}

func (c courseRepository) GetCourse(id uuid.UUID) (*models.Course, error) {
	var course *models.Course
	err := c.db.Preload("Chapters").Preload("Chapters.Sections").First(&course, "id=?", id.String()).Error
	if err != nil {
		return nil, err
	}
	return course, nil
}

func (c courseRepository) GetCourseFromChapter(id uuid.UUID) (*models.Course, error) {
	var chapter *models.Chapter
	err := c.db.Preload("Courses").First(&chapter, "id=?", id.String()).Error

	courseId := chapter.Courses[0].ID
	course, err2 := c.GetCourse(courseId)

	if err != nil || err2 != nil {
		return nil, err
	}

	return course, nil
}

func (c courseRepository) GetSection(id uuid.UUID) (*models.Section, error) {
	var section *models.Section
	err := c.db.First(&section, "id=?", id.String()).Error

	if err != nil {
		return nil, err
	}

	return section, nil
}

func (c courseRepository) GetAllCourses() (*[]models.Course, error) {
	var courses []models.Course
	result := c.db.Preload("Chapters").Preload("Chapters.Sections").Find(&courses)
	if result.Error != nil {
		return nil, result.Error
	}
	return &courses, nil
}

func (c courseRepository) GenerateCourse(coursedto dto.GenerateCourseInput) error {

	return nil
}
